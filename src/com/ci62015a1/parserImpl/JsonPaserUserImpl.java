package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ci62015a1.db.DbObject;
import com.ci62015a1.entity.User;
import com.ci62015a1.paraser.JsonParserService;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JsonPaserUserImpl  implements JsonParserService{
  
	private String fname;
	private String margin =" ";
	private String indent= ">>";
	User user;
	private boolean isYelpingSince= false;
	private boolean isUserId = false;
	private boolean isAvgStars = false;
	private boolean isFans = false;
	private boolean isType = false;
	private boolean isName = false;
	private boolean isReviewCount = false;
	private boolean isFriend = false;
	private boolean isElite = false;
	DbObject dbobj;
	
	public JsonPaserUserImpl(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			doParse(s);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public void processJsonArray(JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		System.out.println(margin + "JSON Array");
		//indent += " ";
		while (!parser.isClosed()) {
			JsonToken token = parser.nextToken();
			if (JsonToken.END_ARRAY.equals(token)) {
				// The en of the array has been reached
				if(isFriend){
					isFriend = false;
				}
				if(isElite){
					isElite = false;
				}
				break;
			}
			processJsonValue(token, parser);
		}
	}

	public void processJsonObject(JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		System.out.println(margin + "JSON Object");
		//indent += " ";
		while (!parser.isClosed()) {
			JsonToken token = parser.nextToken();
			if (JsonToken.END_OBJECT.equals(token)) {
				// The end of the JSON object has been reached
				break;
			}
			if (!JsonToken.FIELD_NAME.equals(token)) {
				System.out.println("Error. Expected a field name");
				break;
			}
			System.out.println(indent + "Field: " + parser.getCurrentName());
			if(parser.getCurrentName().equals("yelping_since")){
				isYelpingSince = true;
			} else if (parser.getCurrentName().equals("user_id")) {
				isUserId = true;
			} else if (parser.getCurrentName().equals("average_stars")) {
				isAvgStars = true;
			} else if (parser.getCurrentName().equals("fans")) {
				isFans = true;
			} else if (parser.getCurrentName().equals("type")) {
				isType = true;
			} else if (parser.getCurrentName().equals("name")) {
				isName = true;
			} else if (parser.getCurrentName().equals("review_count")) {
				isReviewCount = true;
			} else if (parser.getCurrentName().equals("friends")) {
				isFriend = true;
			} else if (parser.getCurrentName().equals("elite")) {
				isElite = true;
			}
			token = parser.nextToken();
			processJsonValue(token, parser);
		}
	}

	public void processJsonValue(JsonToken token, JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		if (JsonToken.START_OBJECT.equals(token)) {
			processJsonObject(parser);
		} else if (JsonToken.START_ARRAY.equals(token)) {
			processJsonArray(parser);
		} else {
//			if(isYelpingSince){
//				user.setYelping_since(parser.getValueAsString());
//				isYelpingSince = false;
//			}else if (isUserId) {
//				user.setUser_id(parser.getValueAsString());
//				isUserId = false;
//			} else if (isAvgStars) {
//				user.setAverage_stars(parser.getValueAsString());
//				isAvgStars = false;
//			} else if (isFans) {
//				user.setFans(parser.getValueAsString());
//				isFans = false;
//			} else if (isType) {
//				user.setType(parser.getValueAsString());
//				isType = false;
//			} else if (isName) {
//				user.setName(parser.getValueAsString());
//				isName = false;
//			} else if (isReviewCount) {
//				user.setReview_count(parser.getValueAsString());
//				isReviewCount = false;
//			} else if (isFriend) {
//					user.setFriends(parser.getValueAsString());
//			} else if (isElite) {
//				user.setElite(parser.getValueAsString());
//		   }
			System.out.println(margin + "Value: " + parser.getValueAsString());
		}
		
	}

	public void doParse(String fname) {
		this.fname = fname;
		JsonFactory jsonfactory = new JsonFactory();
		try {
			File source = new File(this.fname);
			JsonParser parser = jsonfactory.createParser(source);
			dbobj = new DbObject();
			int count = 1;
			while (!parser.isClosed()) {
			    // read the next element
			    JsonToken token = parser.nextToken();
			    // if the call to nextToken returns null, the end of the file has been reached
			    if (token == null)
			        break;
			    user = new User();
			    processJsonValue(token, parser);
			    dbobj.InsertUser(user);
			    /*count++;
			    if(count >10){
			    	break;
			    }*/
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Parsing failed");
			System.exit(-1);
		}
		
	}

}
