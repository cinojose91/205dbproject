package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import com.ci62015a1.db.DbConnector;
import com.ci62015a1.entity.Checkin;
import com.ci62015a1.entity.User;
import com.ci62015a1.paraser.JsonParserServiceV1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParserCheckinImpl implements JsonParserServiceV1 {

	
	private static final int batchSize = 30000;
	private static Connection con = null;
	private String fname = null;
	
	public  JsonParserCheckinImpl() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			fname = s;
			doParsing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	@Override
	public void doParsing() {
		// TODO Auto-generated method stub
		int checkinCount = 0;
		PreparedStatement psCheckin = null;
		String insertCheckinStatement = "INSERT INTO checkin_info (business_id,checkin_date_time,checkin_value) "
				+ "VALUES (?,?,?)";
		
		try (BufferedReader br = new BufferedReader(new FileReader(fname))) {
			
			String line;
			long start = System.currentTimeMillis();

			con = DbConnector.getInstance().getConnection();
			con.setAutoCommit(false);
			psCheckin = con.prepareStatement(insertCheckinStatement);
			
			while ((line = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				Checkin checkin = mapper.readValue(line, Checkin.class);
				
				for(String key : checkin.getCheckin_info().keySet() ){
					psCheckin.setString(1, checkin.getBusiness_id());
					psCheckin.setString(2, key);
					psCheckin.setInt(3, checkin.getCheckin_info().get(key));
					psCheckin.addBatch();

					if (++checkinCount % batchSize == 0) {
						psCheckin.executeBatch();
						 con.commit();
						 System.out.println(checkinCount +" records are in Checkin table");
					}
					
				}
			}
			
			psCheckin.executeBatch();
			con.commit();
			System.out.println(checkinCount + " records are in Checkin table");

			System.out.println("Total time: " + (System.currentTimeMillis() - start) + " ms");
			con.close();
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	
}
