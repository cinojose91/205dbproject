package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.ci62015a1.db.DBConnection;
import com.ci62015a1.db.DbConnector;
import com.ci62015a1.entity.User;
import com.ci62015a1.paraser.JsonParserServiceV1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParserUserImplV1 implements JsonParserServiceV1 {

	private static final int batchSize = 30000;
	private static Connection con = null;
	private String fname = null;

	public JsonParserUserImplV1() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			fname = s;
			doParsing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void doParsing() {
		// TODO Auto-generated method stub
		int count = 0;
		int friendCount = 0;
		int eliteCount = 0;
		int complementCount = 0;
		int voteCount = 0;
		PreparedStatement psUser = null;
		PreparedStatement psFriend = null;
		PreparedStatement psElite = null;
		PreparedStatement psComplement = null;
		PreparedStatement psVote = null;
		String insertUserStatement = "INSERT INTO user (user_id,yelping_since,average_stars,fans,review_count,name,type) "
				+ "VALUES (?,?,?,?,?,?,?)";
		String insertFriendStatement = "INSERT INTO friends (user_id,friend_id) " + "VALUES (?,?)";
		String inserEliteStatement = "INSERT INTO user_elite (user_id,elite_year)" + "VALUES(?,?)";
		String insertComplementStatement = "INSERT into user_complement "
				+ "(user_id,cute_score,profile_score,funny_score,plain_score,writer_score,"
				+ "note_score,photos_score,hot_score,cool_score,more_score) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		String insertVoteStatement = "INSERT into user_vote (user_id,vote_funny,vote_useful,vote_cool)"
				+ "VALUES (?,?,?,?)";
		try (BufferedReader br = new BufferedReader(new FileReader(fname))) {

			String line;
			long start = System.currentTimeMillis();

			con = DBConnection.getConnection();
			con.setAutoCommit(false);
			psElite = con.prepareStatement(inserEliteStatement);
			psUser = con.prepareStatement(insertUserStatement);
			psFriend = con.prepareStatement(insertFriendStatement);
			psComplement = con.prepareStatement(insertComplementStatement);
			psVote = con.prepareStatement(insertVoteStatement);
			while ((line = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				User user = mapper.readValue(line, User.class);

				// ------------------ Friend process -------------------
				for (String friend : user.getFriends()) {

					psFriend.setString(1, user.getUserId());
					psFriend.setString(2, friend);
					psFriend.addBatch();

					if (++friendCount % batchSize == 0) {
						psFriend.executeBatch();
						con.commit();
						System.out.println(friendCount + " records are in friend table");
					}
				}

				for (int year : user.getElite()) {
					psElite.setString(1, user.getUserId());
					psElite.setInt(2, year);
					psElite.addBatch();
					if (++eliteCount % batchSize == 0) {
						psElite.executeBatch();
						con.commit();
						System.out.println(friendCount + " records are in Elite table");
					}
				}

				// prepare insert statement
				psUser.setString(1, user.getUserId());
				psUser.setString(2, user.getYelpingSince());
				psUser.setDouble(3, user.getAverageStars());
				psUser.setInt(4, user.getFans());
				psUser.setInt(5, user.getReviewCount());
				psUser.setString(6, user.getName());
				psUser.setString(7, user.getType());
				// add to a batch
				psUser.addBatch();

				// execute a batch
				if (++count % batchSize == 0) {
					psUser.executeBatch();
					con.commit();
					System.out.println(count + " records are in user table");
				}

				// prepare Complement
				psComplement.setString(1, user.getUserId());
				psComplement.setInt(2,
						((user.getCompliments().getCute() != null) ? user.getCompliments().getCute() : 0));
				psComplement.setInt(3,
						((user.getCompliments().getProfile() != null) ? user.getCompliments().getProfile() : 0));
				psComplement.setInt(4,
						((user.getCompliments().getFunny() != null) ? user.getCompliments().getFunny() : 0));
				psComplement.setInt(5,
						((user.getCompliments().getPlain() != null) ? user.getCompliments().getPlain() : 0));
				psComplement.setInt(6,
						((user.getCompliments().getWriter() != null) ? user.getCompliments().getWriter() : 0));
				psComplement.setInt(7,
						((user.getCompliments().getNote() != null) ? user.getCompliments().getNote() : 0));
				;
				psComplement.setInt(8,
						((user.getCompliments().getPhotos() != null) ? user.getCompliments().getPhotos() : 0));
				;
				psComplement.setInt(9, ((user.getCompliments().getHot() != null) ? user.getCompliments().getHot() : 0));
				psComplement.setInt(10,
						((user.getCompliments().getCool() != null) ? user.getCompliments().getCool() : 0));
				psComplement.setInt(11,
						((user.getCompliments().getMore() != null) ? user.getCompliments().getMore() : 0));
				psComplement.addBatch();
				// execute a batch
				if (++complementCount % batchSize == 0) {
					psComplement.executeBatch();
					con.commit();
					System.out.println(complementCount + " records are in Complement table");
				}

				// Prepate Vote
				psVote.setString(1, user.getUserId());
				psVote.setInt(2, user.getVotes().getFunny());
				psVote.setInt(3, user.getVotes().getUseful());
				psVote.setInt(4, user.getVotes().getCool());
				psVote.addBatch();

				// execute a batch
				if (++voteCount % batchSize == 0) {
					psVote.executeBatch();
					con.commit();
					System.out.println(voteCount + " records are in vote table");
				}
			}

			// for remaining batch queries if total record is odd
			psFriend.executeBatch();
			con.commit();
			System.out.println(friendCount + " records are in friend table");

			psUser.executeBatch();
			con.commit();
			System.out.println(count + " records are in user table");

			psComplement.executeBatch();
			con.commit();
			System.out.println(count + " records are in Complement table");

			psElite.executeBatch();
			con.commit();
			System.out.println(count + " records are in Elite table");

			psVote.executeBatch();
			con.commit();
			System.out.println(count + " records are in Vote table");

			System.out.println("Total time: " + (System.currentTimeMillis() - start) + " ms");
			con.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
