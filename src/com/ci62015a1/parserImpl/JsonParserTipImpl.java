package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ci62015a1.db.DbObject;
import com.ci62015a1.paraser.JsonParserService;
import com.ci62015a1.entity.Tip;
import com.ci62015a1.entity.User;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JsonParserTipImpl implements JsonParserService {

	private String fname;
	private String margin =" ";
	private String indent= ">>";
	Tip tip;
	private boolean isUserId = false;
	private boolean isLikes = false;
	private boolean isText = false;
	private boolean isType = false;
	private boolean isDate = false;
	private boolean isBusinessId = false;

	DbObject dbobj;
	
	public JsonParserTipImpl(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location of tipfile :");
		try {
			s = br.readLine();
			doParse(s);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public void doParse(String fname) {
		// TODO Auto-generated method stub
		this.fname = fname;
		JsonFactory jsonfactory = new JsonFactory();
		try {
			File source = new File(this.fname);
			JsonParser parser = jsonfactory.createParser(source);
			dbobj = new DbObject();
			while (!parser.isClosed()) {
			    // read the next element
			    JsonToken token = parser.nextToken();
			    // if the call to nextToken returns null, the end of the file has been reached
			    if (token == null)
			        break;
			    tip = new Tip();
			    processJsonValue(token, parser);
			    dbobj.InsertTip(tip);;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Parsing failed");
			System.exit(-1);
		}
	}

	public void processJsonArray(JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		System.out.println(margin + "JSON Array");
		while (!parser.isClosed()) {
			JsonToken token = parser.nextToken();
			if (JsonToken.END_ARRAY.equals(token)) {
				// The en of the array has been reached
				break;
			}
			processJsonValue(token, parser);
		}
	}

	public void processJsonObject(JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		System.out.println(margin + "JSON Object");
		//indent += " ";
		while (!parser.isClosed()) {
			JsonToken token = parser.nextToken();
			if (JsonToken.END_OBJECT.equals(token)) {
				// The end of the JSON object has been reached
				break;
			}
			if (!JsonToken.FIELD_NAME.equals(token)) {
				System.out.println("Error. Expected a field name");
				break;
			}
			System.out.println(indent + "Field: " + parser.getCurrentName());
			if(parser.getCurrentName().equals("user_id")){
				isUserId = true;
			} else if (parser.getCurrentName().equals("text")) {
				isText = true;
			} else if (parser.getCurrentName().equals("business_id")) {
				isBusinessId = true;
			} else if (parser.getCurrentName().equals("likes")) {
				isLikes = true;
			} else if (parser.getCurrentName().equals("date")) {
				isDate = true;
			} else if (parser.getCurrentName().equals("type")) {
				isType = true;
			} 
			token = parser.nextToken();
			processJsonValue(token, parser);
		}
	}

	public void processJsonValue(JsonToken token, JsonParser parser) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		if (JsonToken.START_OBJECT.equals(token)) {
			processJsonObject(parser);
		} else if (JsonToken.START_ARRAY.equals(token)) {
			processJsonArray(parser);
		} else {
			if(isBusinessId){
				tip.setBusiness_id(parser.getValueAsString());
				isBusinessId = false;
			}else if (isUserId) {
				tip.setUser_id(parser.getValueAsString());
				isUserId = false;
			} else if (isLikes) {
				tip.setLikes(parser.getValueAsString());
				isLikes = false;
			} else if (isDate) {
				tip.setDate(parser.getValueAsString());
				isDate = false;
			} else if (isType) {
				tip.setType(parser.getValueAsString());
				isType = false;
			} else if (isText) {
				tip.setText(parser.getValueAsString());
				isText = false;
			} 
			System.out.println(margin + "Value: " + parser.getValueAsString());
		}
	}

}
