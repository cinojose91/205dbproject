package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ci62015a1.db.DBConnection;
import com.ci62015a1.entity.Business;
import com.ci62015a1.entity.WorkingHour;
import com.ci62015a1.paraser.JsonParserServiceV1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParserBusinessImpl implements JsonParserServiceV1 {

	private static final int batchSize = 3000;
	private static Connection con = null;
	private String fname = null;

	public JsonParserBusinessImpl() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			fname = s;
			doParsing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void doParsing() {
		
		int businessCount, workingHourCount, categoryCount, neighborhoodCount;
		businessCount = workingHourCount = categoryCount = neighborhoodCount = 0;
		PreparedStatement psBusiness, psWorkingHour, psCategory, psNeighborhood;
		psBusiness = psWorkingHour = psCategory = psNeighborhood = null;
		String insertBusinessStatement = "INSERT INTO business (business_id,full_address,open,city,review_count,name,longitude,state,stars,latitude) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
		String insertWorkingHourStatement = "INSERT INTO business_working_hour (business_id,day,close,open) "
				+ "VALUES (?,?,?,?)";
		String insertCategoryStatement = "INSERT INTO business_category (business_id,category) " + "VALUES (?,?)";
		String insertNeighborhoodStatement = "INSERT INTO business_neighborhood (business_id,neighborhood) "
				+ "VALUES (?,?)";

		try (BufferedReader br = new BufferedReader(new FileReader(fname))) {

			String line;
			long start = System.currentTimeMillis();

			con = DBConnection.getConnection();
			con.setAutoCommit(false);
			psBusiness = con.prepareStatement(insertBusinessStatement);
			psWorkingHour = con.prepareStatement(insertWorkingHourStatement);
			psCategory = con.prepareStatement(insertCategoryStatement);
			psNeighborhood = con.prepareStatement(insertNeighborhoodStatement);

			while ((line = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				Business business = mapper.readValue(line, Business.class);
				String businessId = business.getBusinessId();
				
				// ----------- business ------------
				psBusiness.setString(1, businessId);
				psBusiness.setString(2, business.getFullAddress());
				psBusiness.setBoolean(3, business.getOpen());
				psBusiness.setString(4, business.getCity());
				psBusiness.setDouble(5, business.getReviewCount());
				psBusiness.setString(6, business.getName());
				psBusiness.setDouble(7, business.getLongitude());
				psBusiness.setString(8, business.getState());
				psBusiness.setDouble(9, business.getStars());
				psBusiness.setDouble(10, business.getLatitude());
				psBusiness.addBatch();

				if (++businessCount % batchSize == 0) {
					psBusiness.executeBatch();
					con.commit();
					System.out.println(businessCount + " records are in business table");
				}

				// ----------- business_working_hour ------------
				Map<String, WorkingHour> map = business.getHours().getMap();
				for (Map.Entry<String, WorkingHour> entry : map.entrySet()) {
					psWorkingHour.setString(1, businessId);
					psWorkingHour.setString(2, entry.getKey());
					psWorkingHour.setString(3, entry.getValue().getClose());
					psWorkingHour.setString(4, entry.getValue().getOpen());
					psWorkingHour.addBatch();

					if (++workingHourCount % batchSize == 0) {
						psWorkingHour.executeBatch();
						con.commit();
						System.out.println(workingHourCount + " records are in WorkingHour table");
					}
				}

				// ----------- business_category ------------
				for (String category : business.getCategories()) {

					psCategory.setString(1, businessId);
					psCategory.setString(2, category);
					psCategory.addBatch();

					if (++categoryCount % batchSize == 0) {
						psCategory.executeBatch();
						con.commit();
						System.out.println(categoryCount + " records are in category table");
					}
				}

				// ----------- business_neighborhood ------------
				for (String neighborhood : business.getNeighborhoods()) {

					psNeighborhood.setString(1, businessId);
					psNeighborhood.setString(2, neighborhood);
					psNeighborhood.addBatch();

					if (++neighborhoodCount % batchSize == 0) {
						psNeighborhood.executeBatch();
						con.commit();
						System.out.println(neighborhoodCount + " records are in neighborhood table");
					}
				}
			}

			psBusiness.executeBatch();
			con.commit();
			System.out.println(businessCount + " records are in business table");

			psWorkingHour.executeBatch();
			con.commit();
			System.out.println(workingHourCount + " records are in WorkingHour table");

			psCategory.executeBatch();
			con.commit();
			System.out.println(categoryCount + " records are in category table");

			psNeighborhood.executeBatch();
			con.commit();
			System.out.println(neighborhoodCount + " records are in neighborhood table");

			System.out.println("Total time: " + (System.currentTimeMillis() - start) + " ms");
			con.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
