package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.ci62015a1.db.DbConnector;
import com.ci62015a1.entity.Tip;
import com.ci62015a1.paraser.JsonParserServiceV1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParserTipimplV1 implements JsonParserServiceV1{

	private static final int batchSize = 30000;
	private static Connection con = null;
	private String fname = null;
	
	public  JsonParserTipimplV1() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			fname = s;
			doParsing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	@Override
	public void doParsing() {
		// TODO Auto-generated method stub
		int tipCount = 0;
		PreparedStatement psTip = null;
		String insertTipStatement = "INSERT INTO tip (user_id,business_id,text,date,likes,type) "
				+ "VALUES (?,?,?,?,?,?)";
		
		try (BufferedReader br = new BufferedReader(new FileReader(fname))) {
			
			String line;
			long start = System.currentTimeMillis();

			con = DbConnector.getInstance().getConnection();
			con.setAutoCommit(false);
			psTip = con.prepareStatement(insertTipStatement);
			
			while ((line = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				Tip tip = mapper.readValue(line, Tip.class);
				
				psTip.setString(1, tip.getUser_id());
				psTip.setString(2, tip.getBusiness_id());
				psTip.setString(3, tip.getText());
				psTip.setString(4, tip.getDate());
				psTip.setString(5, tip.getLikes());
				psTip.setString(6, tip.getType());
				psTip.addBatch();
				
				// execute a batch
				if (++tipCount % batchSize == 0) {
					psTip.executeBatch();
					con.commit();
					System.out.println(tipCount +" records are in tip table");
				}
				
				
			}
			
			
			psTip.executeBatch();
			con.commit();
			System.out.println(tipCount + " records are in Tip table");

			System.out.println("Total time: " + (System.currentTimeMillis() - start) + " ms");
			con.close();
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}
