package com.ci62015a1.parserImpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.ci62015a1.db.DBConnection;
import com.ci62015a1.entity.Review;
import com.ci62015a1.entity.ReviewVotes;
import com.ci62015a1.paraser.JsonParserServiceV1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParserReviewImpl implements JsonParserServiceV1 {

	private static final int batchSize = 1000;
	private static Connection con = null;
	private String fname = null;

	public JsonParserReviewImpl() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Location :");
		try {
			s = br.readLine();
			fname = s;
			doParsing();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void doParsing() {
		// TODO Auto-generated method stub
		int reviewCount, voteCount;
		reviewCount = voteCount = 0;
		PreparedStatement psReview, psVote;
		psReview = psVote = null;
		String insertReviewStatement = "INSERT INTO review (user_id,review_id,stars,date,text,business_id) "
				+ "VALUES (?,?,?,?,?,?)";
		String insertVoteStatement = "INSERT INTO review_vote (review_id,vote_funny,vote_useful,vote_cool) "
				+ "VALUES (?,?,?,?)";

		try (BufferedReader br = new BufferedReader(new FileReader(fname))) {

			String line;
			long start = System.currentTimeMillis();

			con = DBConnection.getConnection();
			con.setAutoCommit(false);
			psReview = con.prepareStatement(insertReviewStatement);
			psVote = con.prepareStatement(insertVoteStatement);

			while ((line = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				Review review = mapper.readValue(line, Review.class);

				String reviewId = review.getReviewId();
				ReviewVotes reviewVotes = review.getVotes();
				
				psVote.setString(1, reviewId);
				psVote.setInt(2, reviewVotes.getFunny());
				psVote.setInt(3, reviewVotes.getUseful());
				psVote.setInt(4, reviewVotes.getCool());
				psVote.addBatch();

				if (++voteCount % batchSize == 0) {
					psVote.executeBatch();
					con.commit();
					System.out.println(voteCount + " records are in review_vote table");
				}

				psReview.setString(1, review.getUserId());
				psReview.setString(2, reviewId);
				psReview.setInt(3, review.getStars());
				psReview.setString(4, review.getDate());
				psReview.setString(5, review.getText());
				psReview.setString(6, review.getBusinessId());
				psReview.addBatch();

				if (++reviewCount % batchSize == 0) {
					psReview.executeBatch();
					con.commit();
					System.out.println(reviewCount + " records are in review table");
				}

			}

			psVote.executeBatch();
			con.commit();
			System.out.println(voteCount + " records are in review_vote table");

			psReview.executeBatch();
			con.commit();
			System.out.println(reviewCount + " records are in review table");

			System.out.println("Total time: " + (System.currentTimeMillis() - start) + " ms");
			con.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
