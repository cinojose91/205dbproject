package com.ci62015a1.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.ci62015a1.db.DbObject;
import com.ci62015a1.utils.TableBuilder;

public class RestaurantFinder {

	private int ROW_LIMIT = 30;
	private String category;
	public RestaurantFinder(){
		
		checkInFinder();
		//filterBusiness(); TODO : Not completed yet..
	}
	
	public void filterBusiness(){
		System.out.println("Find business with following features");
		String [] categories = {"Bars","Barbeque","Mexican",
				"Italian","Indian","Wheelchair","Pubs","Caribbean"};
		int count = 0;
		for(String s : categories) {
			System.out.println((""+(++count)+". "+ s));
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String opt = null;
		System.out.println("Options (eg : 1,2)");
		String query = "SELECT * FROM business_category where ";
		try {
			opt = br.readLine();
			for (String retval: opt.split(",")){
				query +=" category like '%"+categories[(Integer.parseInt(retval)-1)]+"%' AND";
		      }
			query = query.substring(0,(query.length()-2));
			System.out.println(""+query);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void checkInFinder(){
		
		String date_time = showFinderMenu();
		if(date_time.equals("")){
			return;
		} else{
			Date datestart = new Date();
			Date dateend;
			String query = "SELECT * from checkin_info left join business on business.business_id = checkin_info.business_id "
					+ "inner join business_category on business.business_id = business_category.business_id where "
					+ "checkin_date_time='"+date_time+"' and business_category.category like '%"+category+"%' order by checkin_value desc LIMIT "+ROW_LIMIT;
			//System.out.println(query);
			DbObject dbobj = new DbObject();
			ResultSet rs = dbobj.ExecuteQuery(query);
			dateend = new Date();
			int count = 0;
			TableBuilder tb = new TableBuilder();
			tb.addRow("No#", "Business Name:", "Checkin Hour", "Checkins ","Category");
			tb.addRow("-----", "-------------", "----------", "--------","---------");
			try {
				while(rs.next()){
					tb.addRow(""+(++count),rs.getString("name"),rs.getString("checkin_date_time"),
							rs.getString("checkin_value"),rs.getString("category"));
				}
				System.out.println(tb);
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Total time:"+Math.abs(datestart.getTime()-dateend.getTime())/1000+"(s)");
			
		}
		
		
	}
	
	public String showFinderMenu(){
		
		String retval="";
		System.out.println("Occupency levels");
		System.out.println("1. Sunday");
		System.out.println("2. Monday");
		System.out.println("3. Tuesday");
		System.out.println("4. Wednesday");
		System.out.println("5. Thursday");
		System.out.println("6. Friday");
		System.out.println("7. Saturday");
		System.out.println("8. Exit");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		System.out.println("Day :");
		try {
			s = br.readLine();
			while (Integer.parseInt(s)>8 || Integer.parseInt(s)<1 ){
				System.out.println("Invalid Day,try again :");
				s = br.readLine();
			}
			if(Integer.parseInt(s)==8){
				return "";
			}else {
				
				System.out.println("Time eg (0, for 00.00 - 01.00)");
				System.out.println("Time eg (24, to exit)");
				String d = br.readLine();
				while(Integer.parseInt(d) < 0 || Integer.parseInt(d) >25){
					System.out.println("Time eg (0, for 00.00 - 01.00)");
					System.out.println("Time eg (24, to exit)");
					System.out.println("Invalid time,try again :");
					d = br.readLine();
				}
				System.out.println("Enter category keyword (e.g. food, shopping, cinema)");
				category = br.readLine();
				if(Integer.parseInt(d)==24){
					return "";
				} else{
					retval = d+"-"+(Integer.parseInt(s)-1);
				}
				
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retval;
	}
	
}
