package com.ci62015a1.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ci62015a1.db.DBConnection;
import com.ci62015a1.utils.TableBuilder;

public class NearbyPlacesFinder {

	private static final int ROW_LIMIT = 30;
	private String category;
	private double latitude, longitude;
	private int distance;

	public NearbyPlacesFinder() {

		System.out.println("Find nearby places by category");
		takeInputs();
		try {
			doQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void takeInputs() {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Enter latitude: ");
			latitude = Double.parseDouble(br.readLine());
			System.out.println("Enter longitude: ");
			longitude = Double.parseDouble(br.readLine());
			System.out.println("Enter maximum distance (km): ");
			distance = Integer.parseInt(br.readLine());
			System.out.println("Enter category keyword (e.g. food, shopping, cinema)");
			category = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void doQuery() throws SQLException {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		String query = "SELECT A.name, B.category, A.latitude, A.longitude, "
				+ "(((acos(sin((?*pi()/180)) * sin((latitude*pi()/180))+cos((?*pi()/180)) * cos((latitude*pi()/180)) * cos(((?-longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance "
				+ "FROM business A " 
				+ "INNER JOIN business_category B ON A.business_id=B.business_id "
				+ "WHERE B.category LIKE ? " 
				+ "HAVING distance <= ? " 
				+ "ORDER BY distance " 
				+ "LIMIT ?";

		try {
			// prepare statement
			con = DBConnection.getConnection();
			preparedStatement = con.prepareStatement(query);
			preparedStatement.setDouble(1, latitude);
			preparedStatement.setDouble(2, latitude);
			preparedStatement.setDouble(3, longitude);
			preparedStatement.setString(4, "%" + category + "%");
			preparedStatement.setInt(5, distance);
			preparedStatement.setInt(6, ROW_LIMIT);
			// preparedStatement.setString(1, "36.128300");
			// preparedStatement.setString(2, "36.128300");
			// preparedStatement.setString(3, "-115.162302");
			// preparedStatement.setString(4, "%" + "food" + "%");
			// preparedStatement.setString(5, "1");
			// preparedStatement.setInt(6, ROW_LIMIT);

			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();

			// print results
			if (rs.next()) {

				TableBuilder tb = new TableBuilder();
				tb.addRow("Name", "Category", "Latitude", "Longitude", "Distance (km)");
				tb.addRow("----------", "----------", "----------", "----------", "----------");
				do {

					String colName = rs.getString("A.name");
					String colCategory = rs.getString("B.category");
					String colLatitude = rs.getString("A.latitude");
					String colLongitude = rs.getString("A.longitude");
					double dDistance = rs.getDouble("distance");
					String colDistance = String.format("%1$,.2f", dDistance);
					tb.addRow(colName, colCategory, colLatitude, colLongitude, colDistance);

				} while (rs.next());

				System.out.println(tb);

			// no results
			} else {
				System.out.println("No results found. Please change your inputs.");
				takeInputs();
				doQuery();
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (con != null)
				con.close();
		}
	}
}
