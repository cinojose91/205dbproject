package com.ci62015a1.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ci62015a1.entity.Tip;
import com.ci62015a1.entity.User;

public class DbObject {

	private DbConnector dbconn;
	PreparedStatement statement;

	public void InsertUser(User user) {

		dbconn = DbConnector.getInstance();
		
		String sql = "INSERT INTO user (user_id,yelping_since,average_stars,fans,review_count,name,type) "
				+ "VALUES (?,?,?,?,?,?,?)";
		System.out.println(user);
		
		 PreparedStatement psUser = null;
	     PreparedStatement psFriend = null;
//		try {
//			statement = dbconn.getConnection().prepareStatement(sql);
//			statement.setString(1,user.getUser_id());
//			statement.setString(2, user.getYelping_since());
//			statement.setString(3,user.getAverage_stars());
//			statement.setString(4, user.getFans());
//			statement.setString(5, user.getReview_count());
//			statement.setString(6, user.getName());
//			statement.setString(7,user.getType());
//			statement.executeUpdate();
//			for(String s : user.getFriends()){
//			 sql = "INSERT INTO friends (user_id,friend_id) "
//						+ "VALUES (?,?)";
//			 statement = dbconn.getConnection().prepareStatement(sql);
//			 statement.setString(1,user.getUser_id());
//			 statement.setString(2,s);
//			 statement.executeUpdate();
//			}
//			for(String s : user.getElite()){
//				 sql = "INSERT INTO user_elite (user_id,elite_year) "
//							+ "VALUES (?,?)";
//				 statement = dbconn.getConnection().prepareStatement(sql);
//				 statement.setString(1,user.getUser_id());
//				 statement.setString(2,s);
//				 statement.executeUpdate();
//			}	
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//            System.exit(0);
//		}
		dbconn.disconnect();
	}
	
	
	public void InsertTip(Tip tip) {

		dbconn = DbConnector.getInstance();
		
		String sql = "INSERT INTO tip (user_id,business_id,likes,date,type,text) "
				+ "VALUES (?,?,?,?,?,?)";
		System.out.println(tip);
		try {
			statement = dbconn.getConnection().prepareStatement(sql);
			statement.setString(1,tip.getUser_id());
			statement.setString(2, tip.getBusiness_id());
			statement.setString(3,tip.getLikes());
			statement.setString(4, tip.getDate());
			statement.setString(5, tip.getType());
			statement.setString(6, tip.getText());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            System.exit(0);
		}
		dbconn.disconnect();
	}
	
	public ResultSet ExecuteQuery(String sql){
		
		dbconn = DbConnector.getInstance();
		ResultSet rs = null;
		try {
			statement = dbconn.getConnection().prepareStatement(sql);
			rs = statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}


}
