package com.ci62015a1.db;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.Connection;

public class DbConnector {
	
	// init database constants
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/yelp_db_1";
   

	private static final String USERNAME = "root";
    private static final String PASSWORD = "123456";
    private static final String MAX_POOL = "250";

    // init connection object
    private Connection connection;
    // init properties object
    private Properties properties;

    private static DbConnector instance = null;
    
    /*
     * Singleton implementation
     */
    public static DbConnector getInstance(){
    	if(instance==null){
    		instance = new DbConnector();
    		instance.connect();
    		return instance;
    	}
    	else{
    		instance.connect();
    		return instance;
    	}
    }
    
    // create properties
    private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }

    // connect database
    public Connection connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    // disconnect database
    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
   	 * @return the connection
   	 */
   	public Connection getConnection() {
   		return connection;
   	}
	
}
