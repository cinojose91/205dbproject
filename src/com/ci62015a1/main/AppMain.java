package com.ci62015a1.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.ci62015a1.application.NearbyPlacesFinder;
import com.ci62015a1.application.RestaurantFinder;
import com.ci62015a1.parserImpl.JsonParserBusinessImpl;
import com.ci62015a1.parserImpl.JsonParserCheckinImpl;

import com.ci62015a1.parserImpl.JsonParserReviewImpl;
import com.ci62015a1.parserImpl.JsonParserTipimplV1;

import com.ci62015a1.parserImpl.JsonParserUserImplV1;

public class AppMain {

	public static void main(String[] args) {
		printMenu();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		try {
			s = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		switch (Integer.parseInt(s)) {
		case 1:
			new JsonParserUserImplV1();
			break;
		case 2:
			new JsonParserBusinessImpl();
			break;
		case 3:
			new JsonParserCheckinImpl();
			break;
		case 4:
			new JsonParserReviewImpl();
			break;
		case 5:
			new JsonParserTipimplV1();
			break;
		case 6 :
			new RestaurantFinder();
			break;
		case 7 :
			new NearbyPlacesFinder();
			break;
		default:
			System.out.println("Invalid Option");
		}

	}

	public static void printMenu() {

		System.out.println("[1] Parse User file");
		System.out.println("[2] Parse business file");
		System.out.println("[3] Parse Checkin file");
		System.out.println("[4] Parse Review file");
		System.out.println("[5] Parse tip file");
		System.out.println("[6] Restaurant finder");
		System.out.println("[7] Nearby places finder");
		System.out.println("Option:");
	}

}
