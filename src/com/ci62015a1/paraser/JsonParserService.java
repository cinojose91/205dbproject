package com.ci62015a1.paraser;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public interface JsonParserService {

	public void doParse(String fname);
	public void processJsonArray(JsonParser parser) throws JsonParseException, IOException;
	public void processJsonObject(JsonParser parser) throws JsonParseException, IOException;
	public void processJsonValue(JsonToken token, JsonParser parser) throws JsonParseException, IOException;
	
}
