
package com.ci62015a1.entity;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "profile",
    "cute",
    "funny",
    "plain",
    "writer",
    "note",
    "photos",
    "hot",
    "cool",
    "more"
})
public class Compliments {

    @JsonProperty("profile")
    private Integer profile;
    @JsonProperty("cute")
    private Integer cute;
    @JsonProperty("funny")
    private Integer funny;
    @JsonProperty("plain")
    private Integer plain;
    @JsonProperty("writer")
    private Integer writer;
    @JsonProperty("note")
    private Integer note;
    @JsonProperty("photos")
    private Integer photos;
    @JsonProperty("hot")
    private Integer hot;
    @JsonProperty("cool")
    private Integer cool;
    @JsonProperty("more")
    private Integer more;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The profile
     */
    @JsonProperty("profile")
    public Integer getProfile() {
        return profile;
    }

    /**
     * 
     * @param profile
     *     The profile
     */
    @JsonProperty("profile")
    public void setProfile(Integer profile) {
        this.profile = profile;
    }

    /**
     * 
     * @return
     *     The cute
     */
    @JsonProperty("cute")
    public Integer getCute() {
        return cute;
    }

    /**
     * 
     * @param cute
     *     The cute
     */
    @JsonProperty("cute")
    public void setCute(Integer cute) {
        this.cute = cute;
    }

    /**
     * 
     * @return
     *     The funny
     */
    @JsonProperty("funny")
    public Integer getFunny() {
        return funny;
    }

    /**
     * 
     * @param funny
     *     The funny
     */
    @JsonProperty("funny")
    public void setFunny(Integer funny) {
        this.funny = funny;
    }

    /**
     * 
     * @return
     *     The plain
     */
    @JsonProperty("plain")
    public Integer getPlain() {
        return plain;
    }

    /**
     * 
     * @param plain
     *     The plain
     */
    @JsonProperty("plain")
    public void setPlain(Integer plain) {
        this.plain = plain;
    }

    /**
     * 
     * @return
     *     The writer
     */
    @JsonProperty("writer")
    public Integer getWriter() {
        return writer;
    }

    /**
     * 
     * @param writer
     *     The writer
     */
    @JsonProperty("writer")
    public void setWriter(Integer writer) {
        this.writer = writer;
    }

    /**
     * 
     * @return
     *     The note
     */
    @JsonProperty("note")
    public Integer getNote() {
        return note;
    }

    /**
     * 
     * @param note
     *     The note
     */
    @JsonProperty("note")
    public void setNote(Integer note) {
        this.note = note;
    }

    /**
     * 
     * @return
     *     The photos
     */
    @JsonProperty("photos")
    public Integer getPhotos() {
        return photos;
    }

    /**
     * 
     * @param photos
     *     The photos
     */
    @JsonProperty("photos")
    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    /**
     * 
     * @return
     *     The hot
     */
    @JsonProperty("hot")
    public Integer getHot() {
        return hot;
    }

    /**
     * 
     * @param hot
     *     The hot
     */
    @JsonProperty("hot")
    public void setHot(Integer hot) {
        this.hot = hot;
    }

    /**
     * 
     * @return
     *     The cool
     */
    @JsonProperty("cool")
    public Integer getCool() {
        return cool;
    }

    /**
     * 
     * @param cool
     *     The cool
     */
    @JsonProperty("cool")
    public void setCool(Integer cool) {
        this.cool = cool;
    }

    /**
     * 
     * @return
     *     The more
     */
    @JsonProperty("more")
    public Integer getMore() {
        return more;
    }

    /**
     * 
     * @param more
     *     The more
     */
    @JsonProperty("more")
    public void setMore(Integer more) {
        this.more = more;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
