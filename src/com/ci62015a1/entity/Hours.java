
package com.ci62015a1.entity;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hours {

    @JsonIgnore
    private Map<String, WorkingHour> map = new HashMap<String, WorkingHour>();

    @JsonAnyGetter
    public Map<String, WorkingHour> getMap() {
        return this.map;
    }

    @JsonAnySetter
    public void setMap(String name, WorkingHour value) {
        this.map.put(name, value);
    }

}
