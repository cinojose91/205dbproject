package com.ci62015a1.entity;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "checkin_info",
    "type",
    "business_id"
})
public class Checkin {

	@JsonProperty("checkin_info")
    private Map<String,Integer> checkin_info = new HashMap<String,Integer>();
	@JsonProperty("type")
	private String type;
	@JsonProperty("business_id")
	private String business_id;
	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	/**
	 * @return the additionalProperties
	 */
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	/**
	 * @param additionalProperties the additionalProperties to set
	 */
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	/**
	 * @return the checkin_info
	 */
	public Map<String, Integer> getCheckin_info() {
		return checkin_info;
	}
	/**
	 * @param checkin_info the checkin_info to set
	 */
	public void setCheckin_info(Map<String, Integer> checkin_info) {
		this.checkin_info = checkin_info;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the business_id
	 */
	public String getBusiness_id() {
		return business_id;
	}
	/**
	 * @param business_id the business_id to set
	 */
	public void setBusiness_id(String business_id) {
		this.business_id = business_id;
	}
	
	
}
