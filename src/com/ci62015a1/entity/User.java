
package com.ci62015a1.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "yelping_since",
    "votes",
    "review_count",
    "name",
    "user_id",
    "friends",
    "fans",
    "average_stars",
    "type",
    "compliments",
    "elite"
})
public class User {

    @JsonProperty("yelping_since")
    private String yelpingSince;
    @JsonProperty("votes")
    private Votes votes;
    @JsonProperty("review_count")
    private Integer reviewCount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("friends")
    private List<String> friends = new ArrayList<String>();
    @JsonProperty("fans")
    private Integer fans;
    @JsonProperty("average_stars")
    private Double averageStars;
    @JsonProperty("type")
    private String type;
    @JsonProperty("compliments")
    private Compliments compliments;
    @JsonProperty("elite")
    private List<Integer> elite = new ArrayList<Integer>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The yelpingSince
     */
    @JsonProperty("yelping_since")
    public String getYelpingSince() {
        return yelpingSince;
    }

    /**
     * 
     * @param yelpingSince
     *     The yelping_since
     */
    @JsonProperty("yelping_since")
    public void setYelpingSince(String yelpingSince) {
        this.yelpingSince = yelpingSince;
    }

    /**
     * 
     * @return
     *     The votes
     */
    @JsonProperty("votes")
    public Votes getVotes() {
        return votes;
    }

    /**
     * 
     * @param votes
     *     The votes
     */
    @JsonProperty("votes")
    public void setVotes(Votes votes) {
        this.votes = votes;
    }

    /**
     * 
     * @return
     *     The reviewCount
     */
    @JsonProperty("review_count")
    public Integer getReviewCount() {
        return reviewCount;
    }

    /**
     * 
     * @param reviewCount
     *     The review_count
     */
    @JsonProperty("review_count")
    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The userId
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The friends
     */
    @JsonProperty("friends")
    public List<String> getFriends() {
        return friends;
    }

    /**
     * 
     * @param friends
     *     The friends
     */
    @JsonProperty("friends")
    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    /**
     * 
     * @return
     *     The fans
     */
    @JsonProperty("fans")
    public Integer getFans() {
        return fans;
    }

    /**
     * 
     * @param fans
     *     The fans
     */
    @JsonProperty("fans")
    public void setFans(Integer fans) {
        this.fans = fans;
    }

    /**
     * 
     * @return
     *     The averageStars
     */
    @JsonProperty("average_stars")
    public Double getAverageStars() {
        return averageStars;
    }

    /**
     * 
     * @param averageStars
     *     The average_stars
     */
    @JsonProperty("average_stars")
    public void setAverageStars(Double averageStars) {
        this.averageStars = averageStars;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The compliments
     */
    @JsonProperty("compliments")
    public Compliments getCompliments() {
        return compliments;
    }

    /**
     * 
     * @param compliments
     *     The compliments
     */
    @JsonProperty("compliments")
    public void setCompliments(Compliments compliments) {
        this.compliments = compliments;
    }

    /**
     * 
     * @return
     *     The elite
     */
    @JsonProperty("elite")
    public List<Integer> getElite() {
        return elite;
    }

    /**
     * 
     * @param elite
     *     The elite
     */
    @JsonProperty("elite")
    public void setElite(List<Integer> elite) {
        this.elite = elite;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
