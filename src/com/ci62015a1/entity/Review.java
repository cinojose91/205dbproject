
package com.ci62015a1.entity;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "votes",
    "user_id",
    "review_id",
    "stars",
    "date",
    "text",
//    "type",
    "business_id"
})
public class Review {

    @JsonProperty("votes")
    private ReviewVotes votes;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("review_id")
    private String reviewId;
    @JsonProperty("stars")
    private Integer stars;
    @JsonProperty("date")
    private String date;
    @JsonProperty("text")
    private String text;
//    @JsonProperty("type")
//    private String type;
    @JsonProperty("business_id")
    private String businessId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The votes
     */
    @JsonProperty("votes")
    public ReviewVotes getVotes() {
        return votes;
    }

    /**
     * 
     * @param votes
     *     The votes
     */
    @JsonProperty("votes")
    public void setVotes(ReviewVotes votes) {
        this.votes = votes;
    }

    /**
     * 
     * @return
     *     The userId
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The reviewId
     */
    @JsonProperty("review_id")
    public String getReviewId() {
        return reviewId;
    }

    /**
     * 
     * @param reviewId
     *     The review_id
     */
    @JsonProperty("review_id")
    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    /**
     * 
     * @return
     *     The stars
     */
    @JsonProperty("stars")
    public Integer getStars() {
        return stars;
    }

    /**
     * 
     * @param stars
     *     The stars
     */
    @JsonProperty("stars")
    public void setStars(Integer stars) {
        this.stars = stars;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

//    /**
//     * 
//     * @return
//     *     The type
//     */
//    @JsonProperty("type")
//    public String getType() {
//        return type;
//    }
//
//    /**
//     * 
//     * @param type
//     *     The type
//     */
//    @JsonProperty("type")
//    public void setType(String type) {
//        this.type = type;
//    }

    /**
     * 
     * @return
     *     The businessId
     */
    @JsonProperty("business_id")
    public String getBusinessId() {
        return businessId;
    }

    /**
     * 
     * @param businessId
     *     The business_id
     */
    @JsonProperty("business_id")
    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
