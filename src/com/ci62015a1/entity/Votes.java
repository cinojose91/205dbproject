
package com.ci62015a1.entity;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "funny",
    "useful",
    "cool"
})
public class Votes {

    @JsonProperty("funny")
    private Integer funny;
    @JsonProperty("useful")
    private Integer useful;
    @JsonProperty("cool")
    private Integer cool;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The funny
     */
    @JsonProperty("funny")
    public Integer getFunny() {
        return funny;
    }

    /**
     * 
     * @param funny
     *     The funny
     */
    @JsonProperty("funny")
    public void setFunny(Integer funny) {
        this.funny = funny;
    }

    /**
     * 
     * @return
     *     The useful
     */
    @JsonProperty("useful")
    public Integer getUseful() {
        return useful;
    }

    /**
     * 
     * @param useful
     *     The useful
     */
    @JsonProperty("useful")
    public void setUseful(Integer useful) {
        this.useful = useful;
    }

    /**
     * 
     * @return
     *     The cool
     */
    @JsonProperty("cool")
    public Integer getCool() {
        return cool;
    }

    /**
     * 
     * @param cool
     *     The cool
     */
    @JsonProperty("cool")
    public void setCool(Integer cool) {
        this.cool = cool;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
