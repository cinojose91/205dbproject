
package com.ci62015a1.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "business_id",
    "full_address",
    "hours",
    "open",
    "categories",
    "city",
    "review_count",
    "name",
    "neighborhoods",
    "longitude",
    "state",
    "stars",
    "latitude",
    "attributes"
//    "type"
})
public class Business {

    @JsonProperty("business_id")
    private String businessId;
    @JsonProperty("full_address")
    private String fullAddress;
    @JsonProperty("hours")
    private Hours hours;
    @JsonProperty("open")
    private Boolean open;
    @JsonProperty("categories")
    private List<String> categories = new ArrayList<String>();
    @JsonProperty("city")
    private String city;
    @JsonProperty("review_count")
    private Integer reviewCount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("neighborhoods")
    private List<String> neighborhoods = new ArrayList<String>();
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("state")
    private String state;
    @JsonProperty("stars")
    private Double stars;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("attributes")
    private Attributes attributes;
//    @JsonProperty("type")
//    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The businessId
     */
    @JsonProperty("business_id")
    public String getBusinessId() {
        return businessId;
    }

    /**
     * 
     * @param businessId
     *     The business_id
     */
    @JsonProperty("business_id")
    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    /**
     * 
     * @return
     *     The fullAddress
     */
    @JsonProperty("full_address")
    public String getFullAddress() {
        return fullAddress;
    }

    /**
     * 
     * @param fullAddress
     *     The full_address
     */
    @JsonProperty("full_address")
    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    /**
     * 
     * @return
     *     The hours
     */
    @JsonProperty("hours")
    public Hours getHours() {
        return hours;
    }

    /**
     * 
     * @param hours
     *     The hours
     */
    @JsonProperty("hours")
    public void setHours(Hours hours) {
        this.hours = hours;
    }

    /**
     * 
     * @return
     *     The open
     */
    @JsonProperty("open")
    public Boolean getOpen() {
        return open;
    }

    /**
     * 
     * @param open
     *     The open
     */
    @JsonProperty("open")
    public void setOpen(Boolean open) {
        this.open = open;
    }

    /**
     * 
     * @return
     *     The categories
     */
    @JsonProperty("categories")
    public List<String> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    @JsonProperty("categories")
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The city
     */
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The reviewCount
     */
    @JsonProperty("review_count")
    public Integer getReviewCount() {
        return reviewCount;
    }

    /**
     * 
     * @param reviewCount
     *     The review_count
     */
    @JsonProperty("review_count")
    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The neighborhoods
     */
    @JsonProperty("neighborhoods")
    public List<String> getNeighborhoods() {
        return neighborhoods;
    }

    /**
     * 
     * @param neighborhoods
     *     The neighborhoods
     */
    @JsonProperty("neighborhoods")
    public void setNeighborhoods(List<String> neighborhoods) {
        this.neighborhoods = neighborhoods;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The stars
     */
    @JsonProperty("stars")
    public Double getStars() {
        return stars;
    }

    /**
     * 
     * @param stars
     *     The stars
     */
    @JsonProperty("stars")
    public void setStars(Double stars) {
        this.stars = stars;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The attributes
     */
    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * 
     * @param attributes
     *     The attributes
     */
    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

//    /**
//     * 
//     * @return
//     *     The type
//     */
//    @JsonProperty("type")
//    public String getType() {
//        return type;
//    }
//
//    /**
//     * 
//     * @param type
//     *     The type
//     */
//    @JsonProperty("type")
//    public void setType(String type) {
//        this.type = type;
//    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
