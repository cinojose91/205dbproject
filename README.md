# Group 6 Project #

This readme contains the steps needed to run the application.

### What are the dependencies? ###

* Java 1.8 jdk
* Mysql Database (5.0 +)
* Eclipse

### How do I get set up? ###

* Unzip the zip file.
* Go to eclipse > File > Import > General > Existin Project into workspace
* Choose the folder where files extracted.
* Click Import
* Click the run on the top to run the application.
* Change the db.properties in the main folder to add the db username, Password and database name
* All the schema files can be found in the src > resource.db folder.
* Just copy and run the Schema files to get the tables populated.

### Main Menu ###

1. [1] Parse User file
2. [2] Parse business file
3. [3] Parse Checkin file
4. [4] Parse Review file
5. [5] Parse tip file
6. [6] Restaurant finder
7. [7] Nearby places finder

* Choose the option to start the application to parse the file and input the location of the the file.
* Choose 6 & 7 once the data is loaded.